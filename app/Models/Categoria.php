<?php

namespace App\Models;

use  \Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table    = 'tb_categoria_produto';
    protected $fillable = ['nome_categoria'];
    
    protected $primaryKey = 'id_categoria_planejamento';
    
    public $timestamps = false;

    public function produtos()
    {
        return $this->hasMany('App\Models\Produto', 'id_categoria_produto');
    }

}