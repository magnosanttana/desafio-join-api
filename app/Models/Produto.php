<?php

namespace App\Models;

use  \Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table    = 'tb_produto';
    protected $fillable = ['id_categoria_produto','nome_produto','valor_produto'];
    protected $primaryKey = 'id_produto';
    
    const CREATED_AT = 'data_cadastro';

    public $timestamps = false;

    public function categoria()
    {
        return $this->belongsTo('App\Models\Categoria', 'id_categoria_produto');
    }

   

}