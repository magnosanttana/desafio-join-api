<?php
namespace App\Services;

use Validator;
use App\Services\BaseService;
use App\Exceptions\ValidatorException;
use App\Models\Categoria;

class ProdutoService extends BaseService
{
    
    public function lists(){
        return $this->model->orderBy('nome_produto', 'asc')->paginate(5);
    }

    public function create ($data){
        
        $data = $this->validate($data);

        $categoria = Categoria::find($data['id_categoria_produto']);
        
        if(!$categoria){
            throw new ValidatorException(['Categoria não encontrada.']);
        }

        try{
            return $this->model->create($data);
            
        }catch(\Exception $e){
            throw new ValidatorException(['Falha ao tentar salvar registro. Tente novamente e se o erro persistir contate o suporte ;) ']);
        }
        
    }

    public function find($id){
        $registro = $this->model->with(['categoria'])->find($id);
        return $registro;
    }

    public function update($data, $id){
        $data = $this->validate($data, $id);

        $registro = $this->find($id);
        if(!$registro){
            throw new ValidatorException(['Registro não localizado.']);
        }

        try{
            $registro->update($data);
            return $registro;

        }catch(\Exception $e){
            throw new ValidatorException(['Falha ao tentar salvar registro. Tente novamente e se o erro persistir contate o suporte ;) ']);
        }
        
    }

    public function delete($id){
        $registro = $this->find($id);
        
        if(!$registro){
            throw new ValidatorException(['Registro não localizado.']);
        }

        try{
            $registro->delete($id);
            return [];

        }catch(\Exception $e){
            throw new ValidatorException(['Falha ao tentar excluir registro. Tente novamente e se o erro persistir contate o suporte ;) ']);
        }
        
    }

    protected function validate($data, $idRegistro = null){
        $rules = [
            'id_categoria_produto'      => 'required',
            'nome_produto'              => 'required|max:150',
            'valor_produto'             => 'required',
        ];

        $validator = Validator::make($data, $rules, [], 
        [
            'id_categoria_produto'      => 'categoria',
            'nome_produto'              => 'nome do produto',
            'valor_produto'             => 'valor do produto'
        ]);

        if ($validator->fails()) {
            throw new ValidatorException($validator->errors()->all());
        }


        $str = str_replace('.', '', $data['valor_produto']); 
        $data['valor_produto'] = str_replace(',', '.', $str);
        
        return $data;

    }
}
