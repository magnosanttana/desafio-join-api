<?php
namespace App\Services;

use Validator;
use App\Services\BaseService;
use App\Exceptions\ValidatorException;

class CategoriaService extends BaseService
{
    
    public function lists(){
        return $this->model->orderBy('nome_categoria', 'asc')->paginate(5);
    }

    public function create ($data){
        $this->validate($data);
        try{
            return $this->model->create($data);
            
        }catch(\Exception $e){
            throw new ValidatorException(['Falha ao tentar salvar registro. Tente novamente e se o erro persistir contate o suporte ;) ']);
        }
        
    }

    public function find($id){
       return $this->model->find($id);
    }

    public function update($data, $id){
        $this->validate($data, $id);

        try{

            $registro = $this->find($id);
            $registro->update($data);

            return $registro;

        }catch(\Exception $e){
            throw new ValidatorException(['Falha ao tentar salvar registro. Tente novamente e se o erro persistir contate o suporte ;) ']);
        }
        
    }

    public function delete($id){
        
        $registro = $this->find($id);
        
        if($registro->produtos->count()){
            throw new ValidatorException(['Não é possivel excluir esta categoria pois há produtos vinculados.']);
        }

        try{
            
            $registro->delete($id);
            return [];

        }catch(\Exception $e){
            //return $e;
            throw new ValidatorException(['Falha ao tentar excluir registro. Tente novamente e se o erro persistir contate o suporte ;) ']);
        }
        
    }

    protected function validate($data, $idRegistro = null){
        $rules = [
            'nome_categoria' => 'required|max:150',
        ];

        $validator = Validator::make($data, $rules, [], 
        [
            'nome_categoria' => 'Nome da Categoria',
        ]);

        if ($validator->fails()) {
            throw new ValidatorException($validator->errors()->all());
        }

    }

}
