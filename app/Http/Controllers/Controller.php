<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $service;

    public function __construct()
    {
        $classExp = explode('\\', get_class($this));
        $className = array_pop($classExp);
        $serviceName = str_replace('Controller', 'Service', $className);
        $this->service = app('App\\Services\\'. $serviceName);
    }

    public function index()
    {
        return $this->service->lists();
    }
    
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    public function show(int $id)
    {
        return $this->service->find($id);
    }

    public function update(Request $request, int $id)
    {
        return $this->service->update($request->all(), $id);
    }

    public function destroy(int $id)
    {
        return $this->service->delete($id);
    }

}
