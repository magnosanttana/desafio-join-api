<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {

    Route::get('/', function () {
        return ['Api Desafio Join'];
    });
    
    Route::get('/categorias', 'CategoriaController@index');
    Route::post('/categorias', 'CategoriaController@store');
    Route::get('/categorias/{id}', 'CategoriaController@show');
    Route::put('/categorias/{id}', 'CategoriaController@update');
    Route::delete('/categorias/{id}', 'CategoriaController@destroy');

    Route::resource('produtos', 'ProdutoController');

});